
#include "ogre_one_class.h"


//-------------------------------------------------------------------------------------
ogre_one_class::ogre_one_class(void)
    : m_root( NULL ),
    m_camera( NULL ),
    m_sceneMgr( NULL ),
    m_window( NULL ),
    m_resourcesCfg( Ogre::StringUtil::BLANK ),
    m_pluginsCfg( Ogre::StringUtil::BLANK ),
    m_trayMgr( NULL ),
    m_cameraMan( NULL ),
    m_detailsPanel( NULL ),
    m_cursorWasVisible( false ),
    m_shutDown( false ),
    m_inputManager( NULL ),
    m_mouse( NULL ),
    m_keyboard( NULL )
{
}

//-------------------------------------------------------------------------------------
ogre_one_class::~ogre_one_class(void)
{
    if( m_trayMgr ) 
		delete m_trayMgr;
    if( m_cameraMan )
		delete m_cameraMan;

    //Remove ourself as a Window listener
    Ogre::WindowEventUtilities::removeWindowEventListener( m_window, this );
    windowClosed( m_window );
    delete m_root;
}

//-------------------------------------------------------------------------------------
bool ogre_one_class::configure(void)
{
    // Show the configuration dialog and initialise the system
    // You can skip this and use root.restoreConfig() to load configuration
    // settings if you were sure there are valid ones saved in ogre.cfg
    if( m_root->showConfigDialog() )
    {
        // If returned true, user clicked OK so initialise
        // Here we choose to let the system create a default rendering window by passing 'true'
        m_window = m_root->initialise( true,
			                           "ogre_one_class Render Window" );

        // Let's add a nice window icon

        return true;
    }
    else
    {
        return false;
    }
}
//-------------------------------------------------------------------------------------
void ogre_one_class::autoConfigure(void)
{
	Ogre::RenderSystem* rndSys = m_root->getRenderSystemByName( "OpenGL Rendering Subsystem" );
	m_root->setRenderSystem( rndSys );
	m_root->initialise( false );
	Ogre::NameValuePairList opts;
	opts["vsync"] = "false";

	// create a rendering window

	m_window = m_root->createRenderWindow( "Ogre one class", 800, 600, false, &opts );
}
//-------------------------------------------------------------------------------------
void ogre_one_class::chooseSceneManager(void)
{
    // Get the SceneManager, in this case a generic one
    m_sceneMgr = m_root->createSceneManager( Ogre::ST_GENERIC );
}
//-------------------------------------------------------------------------------------
void ogre_one_class::createCamera(void)
{
    // Create the camera
    m_camera = m_sceneMgr->createCamera( "PlayerCam" );

    // Position it at 500 in Z direction
    m_camera->setPosition( Ogre::Vector3( 0, 0, 80 ) );
    // Look back along -Z
    m_camera->lookAt( Ogre::Vector3( 0, 0, -300 ) );
    m_camera->setNearClipDistance( 5 );

    m_cameraMan = new OgreBites::SdkCameraMan( m_camera );   // create a default camera controller
}
//-------------------------------------------------------------------------------------
void ogre_one_class::createFrameListener(void)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    m_window->getCustomAttribute( "WINDOW", &windowHnd );
    windowHndStr << windowHnd;
    
	pl.insert( std::make_pair( std::string( "WINDOW" ),
		       windowHndStr.str() ) );

    m_inputManager = OIS::InputManager::createInputSystem( pl );

    m_keyboard = static_cast< OIS::Keyboard* >( 
		m_inputManager->createInputObject( OIS::OISKeyboard, true )
		);

	m_mouse = static_cast< OIS::Mouse* >(
		m_inputManager->createInputObject( OIS::OISMouse, true )
		);

    m_mouse->setEventCallback( this );
    m_keyboard->setEventCallback( this );

    //Set initial mouse clipping size
    windowResized( m_window );

    //Register as a Window listener
    Ogre::WindowEventUtilities::addWindowEventListener( m_window, this );

    m_trayMgr = new OgreBites::SdkTrayManager( "InterfaceName",
		                                       m_window,
											   m_mouse,
											   this );

    m_trayMgr->showFrameStats( OgreBites::TL_BOTTOMLEFT );
    //m_trayMgr->showLogo( OgreBites::TL_BOTTOMRIGHT );
    m_trayMgr->hideCursor();

    // create a params panel for displaying sample details
    Ogre::StringVector items;
    items.push_back( "cam.pX" );
    items.push_back( "cam.pY" );
    items.push_back( "cam.pZ" );
    items.push_back( "" );
    items.push_back( "cam.oW" );
    items.push_back( "cam.oX" );
    items.push_back( "cam.oY" );
    items.push_back( "cam.oZ" );
    items.push_back( "" );
    items.push_back( "Filtering" );
    items.push_back( "Poly Mode" );

    m_detailsPanel = m_trayMgr->createParamsPanel( OgreBites::TL_NONE,
		                                           "DetailsPanel",
												   200,
												   items );

    m_detailsPanel->setParamValue( 9, "Bilinear" );
    m_detailsPanel->setParamValue( 10, "Solid" );
    m_detailsPanel->hide();

    m_root->addFrameListener( this );
}
//-------------------------------------------------------------------------------------
void ogre_one_class::destroyScene(void)
{
}
//-------------------------------------------------------------------------------------
void ogre_one_class::createViewports(void)
{
    // Create one viewport, entire window
    Ogre::Viewport* vp = m_window->addViewport( m_camera );
    vp->setBackgroundColour( Ogre::ColourValue( 0, 0, 0 ) );

    // Alter the camera aspect ratio to match the viewport
    m_camera->setAspectRatio(
        Ogre::Real( vp->getActualWidth() ) / Ogre::Real( vp->getActualHeight() ) 
		);
}
//-------------------------------------------------------------------------------------
void ogre_one_class::setupResources(void)
{
    // Load resource paths from config file
    Ogre::ConfigFile cf;
    cf.load( m_resourcesCfg );

    // Go through all sections & settings in the file
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

    Ogre::String secName, typeName, archName;
    while( seci.hasMoreElements() )
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for ( i = settings->begin(); i != settings->end(); ++i )
        {
            typeName = i->first;
            archName = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
                archName,
				typeName,
				secName );
        }
    }
}
//-------------------------------------------------------------------------------------
void ogre_one_class::createResourceListener(void)
{

}
//-------------------------------------------------------------------------------------
void ogre_one_class::loadResources(void)
{
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}
//-------------------------------------------------------------------------------------
void ogre_one_class::go(void)
{
#ifdef _DEBUG
    m_resourcesCfg = "resources_d.cfg";
    m_pluginsCfg   = "plugins_d.cfg";
#else
    m_resourcesCfg = "resources.cfg";
    m_pluginsCfg   = "plugins.cfg";
#endif // #ifdef _DEBUG

    if ( !setup() )
        return;

    m_root->startRendering();

    // clean up
    destroyScene();
}
//-------------------------------------------------------------------------------------
bool ogre_one_class::setup(void)
{
    m_root = new Ogre::Root( m_pluginsCfg );

    setupResources();

    /*bool carryOn = configure();
    if ( !carryOn )
		return false;*/

	autoConfigure();

    chooseSceneManager();
    createCamera();
    createViewports();

    // Set default mipmap level (NB some APIs ignore this)
    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps( 5 );

    // Create any resource listeners (for loading screens)
    createResourceListener();
    // Load resources
    loadResources();

	// Create the scene
    createScene();

	createFrameListener();

    return true;
};
//-------------------------------------------------------------------------------------
void ogre_one_class::createScene(void)
{
	Ogre::Entity* ogreHead = m_sceneMgr->createEntity( "Head", "ogrehead.mesh" );

	Ogre::SceneNode* headNode = m_sceneMgr->getRootSceneNode()->createChildSceneNode();
	headNode->attachObject( ogreHead );

	// Set ambient light
	m_sceneMgr->setAmbientLight( Ogre::ColourValue( 0.5, 0.5, 0.5 ) );

	// Create a light
	Ogre::Light* l = m_sceneMgr->createLight( "MainLight" );
	l->setPosition( 20, 80, 50 );
}
//-------------------------------------------------------------------------------------
bool ogre_one_class::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
    if( m_window->isClosed() )
        return false;

    if( m_shutDown )
        return false;

    //Need to capture/update each device
    m_keyboard->capture();
    m_mouse->capture();

    m_trayMgr->frameRenderingQueued( evt );

    if ( !m_trayMgr->isDialogVisible() )
    {
        m_cameraMan->frameRenderingQueued( evt );   // if dialog isn't up, then update the camera
        if ( m_detailsPanel->isVisible() )   // if details panel is visible, then update its contents
        {
            m_detailsPanel->setParamValue( 0, 
				Ogre::StringConverter::toString( m_camera->getDerivedPosition().x ) );
            
			m_detailsPanel->setParamValue( 1,
				Ogre::StringConverter::toString( m_camera->getDerivedPosition().y ) );

            m_detailsPanel->setParamValue( 2,
				Ogre::StringConverter::toString( m_camera->getDerivedPosition().z ) );

            m_detailsPanel->setParamValue( 4,
				Ogre::StringConverter::toString( m_camera->getDerivedOrientation().w ) );

            m_detailsPanel->setParamValue( 5,
				Ogre::StringConverter::toString( m_camera->getDerivedOrientation().x ) );

            m_detailsPanel->setParamValue( 6,
				Ogre::StringConverter::toString( m_camera->getDerivedOrientation().y ) );

            m_detailsPanel->setParamValue( 7,
				Ogre::StringConverter::toString( m_camera->getDerivedOrientation().z ) );
        }
    }

    return true;
}
//-------------------------------------------------------------------------------------
bool ogre_one_class::keyPressed( const OIS::KeyEvent &arg )
{
    if (m_trayMgr->isDialogVisible()) return true;   // don't process any more keys if dialog is up

    if (arg.key == OIS::KC_F)   // toggle visibility of advanced frame stats
    {
        m_trayMgr->toggleAdvancedFrameStats();
    }
    else if (arg.key == OIS::KC_G)   // toggle visibility of even rarer debugging details
    {
        if (m_detailsPanel->getTrayLocation() == OgreBites::TL_NONE)
        {
            m_trayMgr->moveWidgetToTray(m_detailsPanel, OgreBites::TL_TOPRIGHT, 0);
            m_detailsPanel->show();
        }
        else
        {
            m_trayMgr->removeWidgetFromTray(m_detailsPanel);
            m_detailsPanel->hide();
        }
    }
    else if (arg.key == OIS::KC_T)   // cycle polygon rendering mode
    {
        Ogre::String newVal;
        Ogre::TextureFilterOptions tfo;
        unsigned int aniso;

        switch (m_detailsPanel->getParamValue(9).asUTF8()[0])
        {
        case 'B':
            newVal = "Trilinear";
            tfo = Ogre::TFO_TRILINEAR;
            aniso = 1;
            break;
        case 'T':
            newVal = "Anisotropic";
            tfo = Ogre::TFO_ANISOTROPIC;
            aniso = 8;
            break;
        case 'A':
            newVal = "None";
            tfo = Ogre::TFO_NONE;
            aniso = 1;
            break;
        default:
            newVal = "Bilinear";
            tfo = Ogre::TFO_BILINEAR;
            aniso = 1;
        }

        Ogre::MaterialManager::getSingleton().setDefaultTextureFiltering(tfo);
        Ogre::MaterialManager::getSingleton().setDefaultAnisotropy(aniso);
        m_detailsPanel->setParamValue(9, newVal);
    }
    else if (arg.key == OIS::KC_R)   // cycle polygon rendering mode
    {
        Ogre::String newVal;
        Ogre::PolygonMode pm;

        switch (m_camera->getPolygonMode())
        {
        case Ogre::PM_SOLID:
            newVal = "Wireframe";
            pm = Ogre::PM_WIREFRAME;
            break;
        case Ogre::PM_WIREFRAME:
            newVal = "Points";
            pm = Ogre::PM_POINTS;
            break;
        default:
            newVal = "Solid";
            pm = Ogre::PM_SOLID;
        }

        m_camera->setPolygonMode(pm);
        m_detailsPanel->setParamValue(10, newVal);
    }
    else if(arg.key == OIS::KC_F5)   // refresh all textures
    {
        Ogre::TextureManager::getSingleton().reloadAll();
    }
    else if (arg.key == OIS::KC_SYSRQ)   // take a screenshot
    {
        m_window->writeContentsToTimestampedFile("screenshot", ".jpg");
    }
    else if (arg.key == OIS::KC_ESCAPE)
    {
        m_shutDown = true;
    }

    m_cameraMan->injectKeyDown(arg);
    return true;
}

bool ogre_one_class::keyReleased( const OIS::KeyEvent &arg )
{
    m_cameraMan->injectKeyUp(arg);
    return true;
}

bool ogre_one_class::mouseMoved( const OIS::MouseEvent &arg )
{
    if (m_trayMgr->injectMouseMove(arg)) return true;
    m_cameraMan->injectMouseMove(arg);
    return true;
}

bool ogre_one_class::mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
    if (m_trayMgr->injectMouseDown(arg, id)) return true;
    m_cameraMan->injectMouseDown(arg, id);
    return true;
}

bool ogre_one_class::mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
    if (m_trayMgr->injectMouseUp(arg, id)) return true;
    m_cameraMan->injectMouseUp(arg, id);
    return true;
}

//Adjust mouse clipping area
void ogre_one_class::windowResized(Ogre::RenderWindow* rw)
{
    unsigned int width, height, depth;
    int left, top;
    rw->getMetrics(width, height, depth, left, top);

    const OIS::MouseState &ms = m_mouse->getMouseState();
    ms.width = width;
    ms.height = height;
}

//Unattach OIS before window shutdown (very important under Linux)
void ogre_one_class::windowClosed(Ogre::RenderWindow* rw)
{
    //Only close for window that created OIS (the main window in these demos)
    if( rw == m_window )
    {
        if( m_inputManager )
        {
            m_inputManager->destroyInputObject( m_mouse );
            m_inputManager->destroyInputObject( m_keyboard );

            OIS::InputManager::destroyInputSystem(m_inputManager);
            m_inputManager = 0;
        }
    }
}

