
#ifndef __ogre_one_class_h_
#define __ogre_one_class_h_

#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <SdkTrays.h>
#include <SdkCameraMan.h>

class ogre_one_class : 
	public Ogre::FrameListener,
	public Ogre::WindowEventListener,
	public  OIS::KeyListener,
	public  OIS::MouseListener,
	  OgreBites::SdkTrayListener
{
public:
    ogre_one_class(void);
    virtual ~ogre_one_class(void);

    void go(void);

protected:
    bool setup();
    bool configure(void);
	void autoConfigure(void);
    void chooseSceneManager(void);
    void createCamera(void);
    void createFrameListener(void);
	void createScene(void);
    void destroyScene(void);
    void createViewports(void);
    void setupResources(void);
    void createResourceListener(void);
    void loadResources(void);

    // Ogre::FrameListener
    virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);

    // OIS::KeyListener
    virtual bool keyPressed( const OIS::KeyEvent &arg );
    virtual bool keyReleased( const OIS::KeyEvent &arg );
    // OIS::MouseListener
    virtual bool mouseMoved( const OIS::MouseEvent &arg );
    virtual bool mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id );
    virtual bool mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id );

    // Ogre::WindowEventListener
    //Adjust mouse clipping area
    virtual void windowResized(Ogre::RenderWindow* rw);
    //Unattach OIS before window shutdown (very important under Linux)
    virtual void windowClosed(Ogre::RenderWindow* rw);

    Ogre::Root *m_root;
    Ogre::Camera* m_camera;
    Ogre::SceneManager* m_sceneMgr;
    Ogre::RenderWindow* m_window;
    Ogre::String m_resourcesCfg;
    Ogre::String m_pluginsCfg;

    // OgreBites
    OgreBites::SdkTrayManager* m_trayMgr;
    OgreBites::SdkCameraMan* m_cameraMan;     // basic camera controller
    OgreBites::ParamsPanel* m_detailsPanel;   // sample details panel
    bool m_cursorWasVisible;                  // was cursor visible before dialog appeared
    bool m_shutDown;

    //OIS Input devices
    OIS::InputManager* m_inputManager;
    OIS::Mouse*    m_mouse;
    OIS::Keyboard* m_keyboard;
};

#endif // #ifndef __ogre_one_class_h_
